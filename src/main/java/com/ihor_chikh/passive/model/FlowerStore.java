package com.ihor_chikh.passive.model;

import java.util.*;

public class FlowerStore {
    private List<Flower> flowers;
    private List<Flowerpot> flowerpots;

    {
        Flower f1 = new Flower("Rose", 20);
        Flower f2 = new Flower("Cherry Blossom", 30);
        Flower f3 = new Flower("Bleeding Heart", 10);
        Flower f4 = new Flower("Water Lilies", 40);
        Flower f5 = new Flower("Lotus", 50);
        Flowerpot fp1 = new Flowerpot("red", 20);
        Flowerpot fp2 = new Flowerpot("green", 12);
        Flowerpot fp3 = new Flowerpot("blue", 14);

        flowers = new ArrayList<>();
        flowers.add(f1);
        flowers.add(f2);
        flowers.add(f3);
        flowers.add(f4);
        flowers.add(f5);
        flowerpots = new ArrayList<>();
        flowerpots.add(fp1);
        flowerpots.add(fp2);
        flowerpots.add(fp3);
    }

    public FlowerStore() {

    }

    public FlowerStore(List flowers, List flowerpots) {
        this.flowers = flowers;
        this.flowerpots = flowerpots;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public List<Integer> getFlowerPrices() {
        List<Integer> prices = new ArrayList<>();
        for (int i = 0; i < flowers.size(); i++) {
            prices.add(flowers.get(i).getPrice());
        }
        return prices;
    }

    public List<Integer> getFlowerPrices(List<Flower> flowers) {
        List<Integer> prices = new ArrayList<>();
        for (int i = 0; i < flowers.size(); i++) {
            prices.add(flowers.get(i).getPrice());
        }
        return prices;
    }

    public List<Integer> getFlowerpotsPrices() {
        List<Integer> prices = new ArrayList<>();
        for (int i = 0; i < flowerpots.size(); i++) {
            prices.add(flowerpots.get(i).getPrice());
        }
        return prices;
    }

    public List<Integer> getFlowerpotsPrices(List<Flowerpot> flowerpots) {
        List<Integer> prices = new ArrayList<>();
        for (int i = 0; i < flowerpots.size(); i++) {
            prices.add(flowerpots.get(i).getPrice());
        }
        return prices;
    }

    public List<Flowerpot> getFlowerpots() {
        return flowerpots;
    }

    public List<Flower> sortFlowersByPrice() {
        flowers.sort(Comparator.comparing(Flower::getPrice));
        return flowers;
    }

    public List<Flower> sortFlowersByPrice(List flowers) {
        flowers.sort(Comparator.comparing(Flower::getPrice));
        return flowers;
    }

    public List<Flowerpot> sortFlowerpotsByPrice() {
        flowerpots.sort(Comparator.comparing(Flowerpot::getPrice));
        return flowerpots;
    }

    public List<Flowerpot> sortFlowerpotsByPrice(List flowerpots) {
        flowerpots.sort(Comparator.comparing(Flowerpot::getPrice));
        return flowerpots;
    }

    public int getFlowersCheck(List<Integer> flowersQty) {
        int check = 0;
        for (int i = 0; i < flowers.size(); i++) {
            check = check + (flowersQty.get(i) * flowers.get(i).getPrice());
        }
        return check;
    }
}
