package com.ihor_chikh.passive.view;

import com.ihor_chikh.passive.model.Flower;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View {

    private Scanner scanner = new Scanner(System.in);

    public View() {

    }

    public void startView() {
        System.out.println("Ви заходите в магазин квітів." + "\n"
                + "Перед вами стоїть невеликий квітковий магазин в доволі витриманому стилі." + "\n"
                + "Ви відкриваєте двері." + "\n"
                + "Вони скриплять, але вас це не сильно дратує." + "\n"
                + "Ви оглядуєтесь навколо." + "\n"
                + "Інтер'єр магазину затишний та викликає довіру. Атмосферу магазину вповні можна назвати невимушеною." + "\n"
                + "Ви відчуваєте цей запах квітів, ніжний та приємний." + "\n"
                + "З за столу встає дівчина і ви не можете звести з неї погляду, вона надзвичайно красива." + "\n"
                + "Вона запитує" + "\n"
                + "\n"
                + "-- Привіт, мене звати Аліса. Чим можу допомогти? ");
    }

    public void endView() {
        System.out.println("-- До зустрічі!)");
    }

    public int chooseAction() {
        System.out.println("\n1  - Подивитись квіти;" +
                "\n2  - Подивитись горшки;" +
                "\n3  - Прайслист квітів;" +
                "\n33 - Посортувати квіти по ціні;" +
                "\n4  - Прайслист горшків;" +
                "\n44 - Посортувати горшки по ціні;" +
                "\n5  - Зробити букет;" +
                "\n0  - Вийти з магазину.");
        return scanner.nextInt();
    }

    public void showAvailableFlowers(List flowers) {
        System.out.println("Available flowers: ");
        for (int i = 0; i < flowers.size(); i++) {
            System.out.println(flowers.get(i));
        }
    }

    public void showAvailableFlowers(List flowers, List prices) {
        System.out.println("Available flowers: ");
        for (int i = 0; i < flowers.size(); i++) {
            System.out.println(flowers.get(i) + " - " + prices.get(i));
        }
    }

    public void showAvailableFlowerpots(List flowerpots) {
        System.out.println("Available flowerpots: ");
        for (int i = 0; i < flowerpots.size(); i++) {
            System.out.println(flowerpots.get(i));
        }
    }

    public void showAvailableFlowerpots(List flowerpots, List prices) {
        System.out.println("Available flowerpots: ");
        for (int i = 0; i < flowerpots.size(); i++) {
            System.out.println(flowerpots.get(i) + " - " + prices.get(i));
        }
    }

    public List<Integer> doABouquet(List<Flower> flowers) {
        System.out.println("Let's do a bouquet!");
        List<Integer> qty = new ArrayList<>();
        for (int i = 0; i < flowers.size(); i++) {
            System.out.println("How many " + flowers.get(i) + " flowers do you want?");
            qty.add(scanner.nextInt());
        }
        return qty;
    }

    public void bouquetPrice(int price) {
        System.out.println("Done! It will cost " + price);
    }
}
