package com.ihor_chikh.passive.controller;

import com.ihor_chikh.passive.model.FlowerStore;
import com.ihor_chikh.passive.view.View;

import java.util.List;

public class Controller {
    private FlowerStore model;
    private View view;

    {
        model = new FlowerStore();
        view = new View();
    }

    public Controller() {
        startView();
    }

    public Controller(FlowerStore model, View view) {
        this.model = model;
        this.view = view;
    }

    private void startView() {
        view.startView();
        int action;
        do {
            action = view.chooseAction();
            if (action == 1) {
                view.showAvailableFlowers(model.getFlowers());
            }
            if (action == 2) {
                view.showAvailableFlowerpots(model.getFlowerpots());
            }
            if (action == 3) {
                view.showAvailableFlowers(model.getFlowers(), model.getFlowerPrices());
            }
            if (action == 33) {
                view.showAvailableFlowers(model.sortFlowersByPrice(), model.getFlowerPrices());
            }
            if (action == 4) {
                view.showAvailableFlowerpots(model.getFlowerpots(), model.getFlowerpotsPrices());
            }
            if (action == 44) {
                view.showAvailableFlowerpots(model.sortFlowerpotsByPrice(), model.getFlowerpotsPrices());
            }
            if (action == 5) {
                List flowersQty = view.doABouquet(model.getFlowers());
                view.bouquetPrice(model.getFlowersCheck(flowersQty));
            }
        }
        while (action != 0);
        view.endView();
    }
}
